package com.mastek.emp.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.mastek.emp.domain.Employee;
import com.mastek.emp.repository.EmployeeRepo;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class EmployeeService {

	private Map<Long, Employee> employees = EmployeeRepo.getEmployees();

	public EmployeeService() {
		// Initiating with default employees for testing
		employees.put(1L, new Employee(1L, 1L, "John", "Flower", "john.flower@email.com"));
		employees.put(2L, new Employee(2L, 1L, "Martin", "Fanthome", "martin.fanthome@email.com"));
	}

	@HystrixCommand(fallbackMethod = "fallbackEmployees")
	public List<Employee> getAllEmployees() {
		return new ArrayList<Employee>(employees.values());
	}

	public Employee getEmployeeById(long id) {
		return employees.get(id);
	}

	public Employee addEmployee(Employee employee) {
		employee.setId(employees.size() + 1L);
		employees.put(employee.getId(), employee);
		return employee;
	}

	public Employee updateEmployee(long id, Employee employee) {
		Employee employeeToUpdate = employees.get(id);
		if (employeeToUpdate == null) {
			return null;
		}

		employeeToUpdate.setDeptId(employee.getDeptId());
		employeeToUpdate.setFirstName(employee.getFirstName());
		employeeToUpdate.setLastName(employee.getLastName());
		employeeToUpdate.setEmail(employee.getEmail());
		employees.put(id, employeeToUpdate);
		return employeeToUpdate;
	}

	public Employee removeEmployee(long id) {
		return employees.remove(id);
	}

	public List<Employee> fallbackEmployees() {
		return Collections.emptyList();
	}

}
