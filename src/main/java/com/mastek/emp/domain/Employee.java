package com.mastek.emp.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat
public class Employee {

	private long id;
	private long deptId;
	private String firstName;
	private String lastName;
	private String email;
	
	public Employee() {
		// TODO Auto-generated constructor stub
	}
	
	
	public Employee(long id, long deptId, String firstName, String lastName, String email) {
		this.id = id;
		this.deptId = deptId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}
	
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getDeptId() {
		return deptId;
	}

	public void setDeptId(long deptId) {
		this.deptId = deptId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
