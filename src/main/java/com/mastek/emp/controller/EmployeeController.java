package com.mastek.emp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.mastek.emp.domain.Employee;
import com.mastek.emp.exceptions.InvalidInputException;
import com.mastek.emp.service.EmployeeService;

@RestController
@RequestMapping(value = "/employees")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	@Autowired
	RestTemplate restTemplate;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<Employee>> getEmployees() {
		return new ResponseEntity<List<Employee>>(employeeService.getAllEmployees(), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Employee> addEmployee(@RequestBody Employee employee) {

		// TODO service intercommunication using event sourcing

		Long deptId = employee.getDeptId();
		Department department = restTemplate.getForObject("http://localhost:8096/departments/{deptId}",
				Department.class, deptId);
		if (department == null) {
			throw new InvalidInputException("Invalid Department ID");
		}
		employee.setDeptId(department.getId());

		return new ResponseEntity<Employee>(employeeService.addEmployee(employee), HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = "application/json", path = "/{id}")
	public ResponseEntity<Employee> updateEmployee(@PathVariable("id") long id, @RequestBody Employee employee) {
		return new ResponseEntity<Employee>(employeeService.updateEmployee(id, employee), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json", path = "/{id}")
	public void deleteEmployee(@PathVariable("id") long id) {
		employeeService.removeEmployee(id);
	}
	
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}

}

class Department {
	private long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}


