package com.mastek.emp.repository;

import java.util.HashMap;
import java.util.Map;

import com.mastek.emp.domain.Employee;

public class EmployeeRepo {

	private static Map<Long, Employee> employees = new HashMap<>();

	public static Map<Long, Employee> getEmployees() {
		return employees;
	}

}
