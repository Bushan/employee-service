package com.mastek.emp.exceptions;

public class InvalidInputException extends RuntimeException {

	private static final long serialVersionUID = -180104172938508504L;

	public InvalidInputException() {
		super("Invalid Input");
	}

	public InvalidInputException(String message) {
		super(message);
	}
}