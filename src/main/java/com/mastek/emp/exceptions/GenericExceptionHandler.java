package com.mastek.emp.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class GenericExceptionHandler {

	@ExceptionHandler(Exception.class)
	protected ResponseEntity<ErrorMessage> handleInvalidInput(RuntimeException ex) {
		// TODO
		return new ResponseEntity<ErrorMessage>(new ErrorMessage("Generic Exception"), HttpStatus.BAD_REQUEST);
	}

}
