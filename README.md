# Employee Service 

This is sample service for employees

## Employee Server Sample

Running from CLI
 
	$ java -jar employee-service-0.0.1.jar

or, Running main method class 'EmployeeServiceApplication.java' from IDE or CLI

Its configured to run on port '8095'
This gets auto register with Eureka Discovery service to serve the Eureka API from "/eureka".

## Resources

	POST http://${host}:${port}/employees/
	{
		"deptId": 1,
		"firstName": "John",
		"lastName": "Edwards",
		"email": "jEdwards@email.com"
	}

	GET http://${host}:${port}/employees/
	
	PUT http://${host}:${port}/employees/{employee_Id}
	
	{
		"deptId": 1,
		"firstName": "Alex",
		"lastName": "Edwards",
		"email": "aEdwards@email.com"
	}
  
	DELETE http://${host}:${port}/employees/{employee_Id}
	
## Hystrix
To monitor hystrix stream service circuiteBreaker is enabled and can be monitor at URL, use same url to monitor on Hystrix Dashboard
	
	http://${host}:${port}/hystrix.stream

## Docker

Running in Docker container

	$ docker run -p 8095:8095 -e "EUREKA_SERVER_HOST=<server IP>" -e "EUREKA_SERVER_PORT=<server port e.g. 8761>" -t <image-prefix>/employee-service:<image-tag>

This is configured to build docker image and push to private nexus repository using maven plugin.
	
	$ mvn clean -X package docker:build -DpushImage

Maven properties

	docker.image.prefix=<nexus host>:18079
	docker.registry.id=<Docker Repository name>
	registry.url=<registry url e.g. https://192.168.1.98:8449/repository/myDockerRepo/ >